CKEDITOR.plugins.add('variables', {
    icons: 'variables',
    hidpi: true,
    init: function (editor) {
        // Définissez le comportement de votre bouton ici
        editor.addCommand('variables', {
            exec: function (editor) {
                console.log('variable added')
            }
        });
        // Register dialog.
        CKEDITOR.dialog.add( 'variables', this.path + 'dialogs/variables.js' );

        // Put ur init code here.
        editor.widgets.add( 'variables', {
            // Widget code.
            dialog: 'variables',
            pathName: 'espace réservé',
            // We need to have wrapping element, otherwise there are issues in
            // add dialog.
            template: '<span class="cke_variables">[[]]</span>',

            downcast: function() {
                return new CKEDITOR.htmlParser.text('[[' + this.data.name + ']]');
            },

            init: function() {
                // Note that placeholder markup characters are stripped for the name.
                this.setData( 'name', this.element.getText().slice( 2, -2 ) );
            },

            data: function() {
                this.element.setText( '[[' + this.data.name + ']]' );
            },
        } );


        editor.ui.addButton('Variables', {
            label: 'Insérer des variables',
            command: 'variables',
            toolbar: 'insert'
        });
    }
});
