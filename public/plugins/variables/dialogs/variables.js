CKEDITOR.dialog.add( 'variables', function( editor ) {
    var lang = editor.lang.placeholder,
        generalLabel = editor.lang.common.generalTab,
        validNameRegex = /^[^\[\]<>]+$/;
    var variables = [];
    var options = JSON.parse(document.getElementById('variables-data').getAttribute('data-variables'));
    // Ajoutez les options depuis les données du backend
    Object.values(options).forEach(function(option) {
        variables.push([option]);
    })

    return {
        title: 'Insérer une variable',
        minWidth: 300,
        minHeight: 80,
        contents: [
            {
                id: 'info',
                label: generalLabel,
                title: generalLabel,
                elements: [
                    // Dialog window UI elements.
                    {
                        id: 'name',
                        type: 'select',
                        style: 'width: 100%;',
                        label: 'Variables disponibles',
                        items: variables,
                        required: true,
                        setup: function( widget ) {
                            this.setValue( widget.data.name );
                        },
                        commit: function( widget ) {
                            widget.setData( 'name', this.getValue() );
                        }
                    }
                ]
            }
        ]
    };
} );
