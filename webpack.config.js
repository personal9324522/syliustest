const path = require('path');
const Encore = require('@symfony/webpack-encore');
const TerserPlugin = require('terser-webpack-plugin');
const syliusBundles = path.resolve(__dirname, 'vendor/sylius/sylius/src/Sylius/Bundle/');
const uiBundleScripts = path.resolve(syliusBundles, 'UiBundle/Resources/private/js/');
const uiBundleResources = path.resolve(syliusBundles, 'UiBundle/Resources/private/');
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const { PurgeCSSPlugin } = require("purgecss-webpack-plugin");
const glob = require('glob-all');

Encore
    // .addPlugin(new ImageMinimizerPlugin({
    //     minimizer: {
    //         implementation: ImageMinimizerPlugin.sharpMinify,
    //         options: {
    //             encodeOptions: {
    //                 // Your options for `sharp`
    //                 // https://sharp.pixelplumbing.com/api-output
    //                 jpeg: {
    //                     quality: 100,
    //                     mozjpeg: true
    //                 },
    //                 webp: {
    //                     quality: 100,
    //                     mozjpeg: true
    //                 }
    //             }
    //         }
    //     }
    // }))
    // .configureTerserPlugin((options) => {
    //     options.parallel = true;
    //     options.terserOptions = {
    //         output: {
    //             comments: false
    //         }
    //     }
    // })
    // .addPlugin(new PurgeCSSPlugin({
    //     paths: glob.sync([
    //         path.join(__dirname, 'templates/**/*.html.twig'),
    //     ]),
    //     FontFace: true,
    //     defaultExtractor: (content) => {
    //         return content.match(/[\w-/:]+(?<!:)/g) || [];
    //     }
    // }))
    .copyFiles({
        from: 'public/images/',
        to: 'images/[path][name].[ext]',
        pattern: /\.(png|jpg|jpeg|webp)$/
    })
    .setOutputPath('public/build/core/')
    .setPublicPath('/build/core')
    .addEntry('core', './assets/app.js')
    .enableStimulusBridge('./assets/controllers.json')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .autoProvidejQuery()
    .enableVersioning()
    .enablePostCssLoader();


const appConfig = Encore.getWebpackConfig();

appConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
appConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
appConfig.resolve.alias['sylius/bundle'] = syliusBundles;
appConfig.name = 'core';

Encore.reset();

Encore
    .configureTerserPlugin((options) => {
        options.parallel = true;
        options.terserOptions = {
            output: {
                comments: false
            }
        }
    })
    .setOutputPath('public/build/core-admin/')
    .setPublicPath('/build/core-admin')
    .addEntry('core-admin', './assets/app-admin.js')
    .enableStimulusBridge('./assets/controllers.json')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .autoProvidejQuery()
    .enablePostCssLoader();


const appAdminConfig = Encore.getWebpackConfig();

appAdminConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
appAdminConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
appAdminConfig.resolve.alias['sylius/bundle'] = syliusBundles;
appAdminConfig.name = 'core-admin';

Encore.reset();

// Shop config
Encore
    .configureTerserPlugin((options) => {
        options.parallel = true;
        options.terserOptions = {
            output: {
                comments: false
            }
        }
    })
    .addPlugin(new TerserPlugin())
    .setOutputPath('public/build/shop/')
    .setPublicPath('/build/shop')
    .addEntry('shop-entry', './vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Resources/private/entry.js')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader();

const shopConfig = Encore.getWebpackConfig();

shopConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
shopConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
shopConfig.resolve.alias['sylius/bundle'] = syliusBundles;
shopConfig.name = 'shop';

Encore.reset();

// Admin config
Encore
    .configureTerserPlugin((options) => {
        options.parallel = true;
        options.terserOptions = {
            output: {
                comments: false
            }
        }
    })
    .addPlugin(new TerserPlugin())
    .setOutputPath('public/build/admin/')
    .setPublicPath('/build/admin')
    .addEntry('admin-entry', './vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/private/entry.js')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader();

const adminConfig = Encore.getWebpackConfig();

adminConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
adminConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
adminConfig.resolve.alias['sylius/bundle'] = syliusBundles;
adminConfig.externals = Object.assign({}, adminConfig.externals, { window: 'window', document: 'document' });
adminConfig.name = 'admin';

module.exports = [appConfig, shopConfig, adminConfig, appAdminConfig];
