<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231102165131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_product ADD id_confirm_mail INT DEFAULT NULL, ADD id_canceled_mail INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74EEBBEEAE FOREIGN KEY (id_confirm_mail) REFERENCES mails_mail (mail_id)');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74989A7702 FOREIGN KEY (id_canceled_mail) REFERENCES mails_mail (mail_id)');
        $this->addSql('CREATE INDEX IDX_677B9B74EEBBEEAE ON sylius_product (id_confirm_mail)');
        $this->addSql('CREATE INDEX IDX_677B9B74989A7702 ON sylius_product (id_canceled_mail)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74EEBBEEAE');
        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74989A7702');
        $this->addSql('DROP INDEX IDX_677B9B74EEBBEEAE ON sylius_product');
        $this->addSql('DROP INDEX IDX_677B9B74989A7702 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP id_confirm_mail, DROP id_canceled_mail');
    }
}
