<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230912092348 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cms_about (about_id INT AUTO_INCREMENT NOT NULL, about_title VARCHAR(55) DEFAULT NULL, about_description LONGTEXT DEFAULT NULL, about_section VARCHAR(50) DEFAULT NULL, PRIMARY KEY(about_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cms_article (article_id INT AUTO_INCREMENT NOT NULL, article_title VARCHAR(55) DEFAULT NULL, article_html LONGTEXT DEFAULT NULL, article_section VARCHAR(50) DEFAULT NULL, article_position INT DEFAULT NULL,article_imageurl VARCHAR(255) DEFAULT NULL, PRIMARY KEY(article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cms_faq (faq_id INT AUTO_INCREMENT NOT NULL, faq_question VARCHAR(255) DEFAULT NULL, faq_answer LONGTEXT DEFAULT NULL, faq_section VARCHAR(50) DEFAULT NULL, faq_position INT DEFAULT NULL, PRIMARY KEY(faq_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cms_homepage (homepage_id INT AUTO_INCREMENT NOT NULL, homepage_title VARCHAR(55) DEFAULT NULL, homepage_maindescription VARCHAR(255) DEFAULT NULL, homepage_about VARCHAR(400) DEFAULT NULL, PRIMARY KEY(homepage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cms_media (media_id INT AUTO_INCREMENT NOT NULL, media_title VARCHAR(55) DEFAULT NULL, media_description VARCHAR(255) DEFAULT NULL, media_path VARCHAR(255) DEFAULT NULL, media_type VARCHAR(50) DEFAULT NULL, PRIMARY KEY(media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE cms_about');
        $this->addSql('DROP TABLE cms_article');
        $this->addSql('DROP TABLE cms_faq');
        $this->addSql('DROP TABLE cms_homepage');
        $this->addSql('DROP TABLE cms_media');
    }
}
