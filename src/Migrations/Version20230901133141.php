<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230901133141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_order ADD itemsTotalAcompte INT DEFAULT NULL, ADD totalAcompte INT DEFAULT NULL, ADD totalRestant INT DEFAULT NULL, ADD itemsSubtotalAcompte INT DEFAULT NULL, ADD isTotallyPaid TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_order_item ADD acompte INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_order DROP itemsTotalAcompte, DROP totalAcompte, DROP totalRestant, DROP itemsSubtotalAcompte');
        $this->addSql('ALTER TABLE sylius_order_item DROP acompte');
    }
}
