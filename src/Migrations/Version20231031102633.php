<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231031102633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mails_mail (mail_id INT AUTO_INCREMENT NOT NULL, mail_name VARCHAR(55) DEFAULT NULL, mail_object VARCHAR(100) DEFAULT NULL, mail_content LONGTEXT DEFAULT NULL, mail_files LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(mail_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mails_signature (signature_id INT AUTO_INCREMENT NOT NULL, signature_content LONGTEXT DEFAULT NULL, PRIMARY KEY(signature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mails_mail');
        $this->addSql('DROP TABLE mails_signature');
    }
}
