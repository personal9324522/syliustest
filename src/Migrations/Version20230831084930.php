<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230831084930 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_product ADD tax_id INT DEFAULT NULL, ADD priceHT NUMERIC(10, 0) DEFAULT NULL, ADD priceTTC NUMERIC(10, 0) DEFAULT NULL, ADD inventory INT DEFAULT NULL, ADD acompte NUMERIC(10, 0) DEFAULT NULL, ADD disponibility VARCHAR(255) DEFAULT NULL, ADD disponibilityMonth VARCHAR(255) DEFAULT NULL, ADD mandatoryDeclaration TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74B2A824D8 FOREIGN KEY (tax_id) REFERENCES sylius_tax_category (id)');
        $this->addSql('CREATE INDEX IDX_677B9B74B2A824D8 ON sylius_product (tax_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74B2A824D8');
        $this->addSql('DROP INDEX IDX_677B9B74B2A824D8 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP tax_id, DROP priceHT, DROP priceTTC, DROP inventory');
    }
}
