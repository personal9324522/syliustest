<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231020111353 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cms_about ADD about_tags VARCHAR(255) DEFAULT NULL, ADD about_meta_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_article ADD article_tags VARCHAR(255) DEFAULT NULL, ADD article_meta_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_homepage ADD homepage_tags VARCHAR(255) DEFAULT NULL, ADD homepage_meta_description VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cms_about DROP about_tags, DROP about_meta_description');
        $this->addSql('ALTER TABLE cms_article DROP article_tags, DROP article_meta_description');
        $this->addSql('ALTER TABLE cms_homepage DROP homapage_tags, DROP homapage_meta_description');
    }
}
