<?php

declare(strict_types=1);

namespace App\Processor\Order;

use App\Entity\Order\Order;
use Sylius\Component\Core\Payment\Exception\NotProvidedOrderPaymentException;
use Sylius\Component\Core\Payment\Provider\OrderPaymentProviderInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Model\OrderInterface as BaseOrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Sylius\Component\Payment\Model\PaymentInterface;

class OrderAcomptePaymentProcessor implements OrderProcessorInterface
{
    public function __construct(
        private OrderPaymentProviderInterface $orderPaymentProvider,
        private string $targetState = PaymentInterface::STATE_CART,
        /** @var array<string> $unprocessableOrderStates */
    ) {
    }

    public function process(Order|BaseOrderInterface $order): void
    {
        /** @var PaymentInterface $lastPayment */
        $lastPayment = $order->getLastPayment($this->targetState);

        if ($order->isTotallyPaid()) {
            return;
        }

        if (null !== $lastPayment) {
            $this->setAmount($order, $lastPayment);
            return;
        }

        try {
            $newPayment = $this->orderPaymentProvider->provideOrderPayment($order, $this->targetState);
            $order->addPayment($newPayment);
            $this->setAMount($order, $newPayment);
        } catch (NotProvidedOrderPaymentException) {
            return;
        }
    }

    private function setAMount(Order $order, PaymentInterface $payment): void
    {
        // si la commande est au statut panier
        if (OrderInterface::STATE_CART === $order->getState()) {
            // si l'option de paiement en totalité est activée
            if ($order->isTotallyPaid()) {
                // on prend le total de la commande
                $payment->setAmount($order->getTotal());
            } else {
                // sinon on prend le total de l'acompte
                $payment->setAmount($order->getTotalAcompte());
            }
        } else {
            $payment->setAmount($order->getTotalRestant());
        }
    }
}
