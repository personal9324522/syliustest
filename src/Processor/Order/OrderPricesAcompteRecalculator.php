<?php

declare(strict_types=1);

namespace App\Processor\Order;

use App\Entity\Order\Order;
use App\Entity\Order\OrderItem;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Model\OrderInterface as BaseOrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Webmozart\Assert\Assert;

final class OrderPricesAcompteRecalculator implements OrderProcessorInterface
{
    public function process(Order|BaseOrderInterface $order): void
    {
        /** @var \Sylius\Component\Core\Model\OrderInterface $order */
        Assert::isInstanceOf($order, OrderInterface::class);

        if (!$order->canBeProcessed()) {
            return;
        }

        $totalAcompte = 0;
        /** @var OrderItem $item */
        foreach ($order->getItems() as $item) {
            // récupération de l'acompte
            $acompte = $item->getAcompte() ?? 0;
            $item->setAcompte((int)$acompte ?? 0);
            $totalAcompte += $acompte * $item->getQuantity();
        }

        // TODO gestion de la taxe et du montant du shipping et ajouter subTotalRestant
        // mise à jour de l'acompte total
        $order->setTotalAcompte((int)$totalAcompte);
        $order->setItemsTotalAcompte((int)$totalAcompte);
        $order->setItemsSubtotalAcompte((int)$totalAcompte);
        // calcul du restant dû
        $order->setTotalRestant((int) ($order->getTotal() - $totalAcompte));
    }
}
