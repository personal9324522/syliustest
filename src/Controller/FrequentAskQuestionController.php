<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CMS\FrequentAskQuestion;
use App\Enum\SectionEnum;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrequentAskQuestionController extends ResourceController
{

    public function pageAction(): Response
    {
        // récupération de tous les éléments de la table FrequentlyAskedQuestion
        $faqs = $this->repository->findBy(['section' => SectionEnum::MAIN_FAQ], ['position' => 'ASC']);
        if (empty($faqs)) {
            return $this->redirectToRoute('app_shop_homepage_index');
        }

        return $this->render('App/FAQ/index.html.twig', [
            'faqs' => $faqs,
        ]);
    }

    public function updatePositionsAction(Request $request): JsonResponse
    {

        $faqs = $this->getParameterFromRequest($request, 'positions');
        // mise à jour des positions
        foreach ($faqs as $faqData) {
            $faq = $this->repository->find($faqData['id']);
            if ($faq instanceof FrequentAskQuestion) {
                $faq->setPosition((int) $faqData['position']);
                $this->manager->persist($faq);
                $this->manager->flush();
            }
        }

        return new JsonResponse();
    }

    private function getParameterFromRequest(Request $request, string $key)
    {
        if ($request !== $result = $request->attributes->get($key, $request)) {
            return $result;
        }

        if ($request->query->has($key)) {
            return $request->query->all()[$key];
        }

        if ($request->request->has($key)) {
            return $request->request->all()[$key];
        }

        return null;
    }
}
