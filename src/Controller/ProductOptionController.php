<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product\Product;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\Exception\UpdateHandlingException;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductOptionController extends ResourceController
{
    public function updateAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        $option = $this->findOr404($configuration);

        $form = $this->resourceFormFactory->create($configuration, $option);
        $form->handleRequest($request);

        if (
            in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'], true) &&
            $form->isSubmitted() &&
            $form->isValid()
        ) {
            $option = $form->getData();

            /** @var ResourceControllerEvent $event */
            $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::UPDATE, $configuration, $option);

            if ($event->isStopped() && !$configuration->isHtmlRequest()) {
                throw new HttpException($event->getErrorCode(), $event->getMessage());
            }
            if ($event->isStopped()) {
                $this->flashHelper->addFlashFromEvent($configuration, $event);

                $eventResponse = $event->getResponse();
                if (null !== $eventResponse) {
                    return $eventResponse;
                }

                return $this->redirectHandler->redirectToResource($configuration, $option);
            }

            try {
                $this->resourceUpdateHandler->handle($option, $configuration, $this->manager);
            } catch (UpdateHandlingException $exception) {
                if (!$configuration->isHtmlRequest()) {
                    return $this->createRestView($configuration, $form, $exception->getApiResponseCode());
                }

                $this->flashHelper->addErrorFlash($configuration, $exception->getFlash());

                return $this->redirectHandler->redirectToReferer($configuration);
            }

            if ($configuration->isHtmlRequest()) {
                $this->flashHelper->addSuccessFlash($configuration, ResourceActions::UPDATE, $option);
            }

            $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::UPDATE, $configuration, $option);

            if (!$configuration->isHtmlRequest()) {
                if ($configuration->getParameters()->get('return_content', false)) {
                    return $this->createRestView($configuration, $option, Response::HTTP_OK);
                }

                return $this->createRestView($configuration, null, Response::HTTP_NO_CONTENT);
            }

            $postEventResponse = $postEvent->getResponse();
            if (null !== $postEventResponse) {
                return $postEventResponse;
            }
            // récupération des Products associés à l'option
            $productService = $this->get('sylius.repository.product');
            $products = $productService->findByOption($option);
            // récupération des optionsVAlue de l'option
            $optionValues = $option->getValues();
            /** @var Product $product */
           foreach ($products as $product) {
               // récupération des variantes du produit
               $variants = $product->getVariants();
               foreach ($variants as $variant) {
                   // récupération des options de la variante
                   $variantOptionsValues = $variant->getOptionValues();
                   foreach($optionValues as $optionValue) {
                       if($variantOptionsValues->contains($optionValue)) {
                           // récupération du channelPricing de la variante
                           $channelPricings = $variant->getChannelPricings();
                           foreach ($channelPricings as $channelPricing) {
                               // mise à jour du prix du channel
                               $price = $product->getPriceTTC();
                               $priceOption = $optionValue->getPrice() ?: 0;
                               $price += $priceOption;
                               $channelPricing->setPrice((int)$price);
                           }
                       }
                   }
               }
               $product->setUpdatedAt(new \DateTime());

               $this->manager->persist($product);
           }

            return $this->redirectHandler->redirectToResource($configuration, $option);
        }
        if (in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'], true) && $form->isSubmitted() && !$form->isValid()) {
            $responseCode = Response::HTTP_UNPROCESSABLE_ENTITY;
        }

        if (!$configuration->isHtmlRequest()) {
            return $this->createRestView($configuration, $form, Response::HTTP_BAD_REQUEST);
        }

        $initializeEvent = $this->eventDispatcher->dispatchInitializeEvent(ResourceActions::UPDATE, $configuration, $option);
        $initializeEventResponse = $initializeEvent->getResponse();
        if (null !== $initializeEventResponse) {
            return $initializeEventResponse;
        }

        return $this->render($configuration->getTemplate(ResourceActions::UPDATE . '.html'), [
            'configuration' => $configuration,
            'metadata' => $this->metadata,
            'resource' => $option,
            $this->metadata->getName() => $option,
            'form' => $form->createView(),
        ], null, $responseCode ?? Response::HTTP_OK);
    }
}
