<?php

declare(strict_types=1);

namespace App\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaxonController extends ResourceController
{
    public function indexAction(Request $request, ?string $category = null): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::INDEX);
        $taxons = $this->resourcesCollectionProvider->get($configuration, $this->repository);

        $event = $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $taxons);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($configuration->getTemplate(ResourceActions::INDEX . '.html'), [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'taxons' => $taxons,
                'category' => $category,
                $this->metadata->getPluralName() => $taxons,
            ]);
        }

        return $this->createRestView($configuration, $taxons);
    }
}
