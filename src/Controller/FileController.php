<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileController extends AbstractController
{
    public function download(Request $request): BinaryFileResponse
    {
        // récupération du nom du fichier
        $path = $request->get('filename');

        // si aucun fichier n'existe
        if (empty($path)) {
            throw new \Exception('Aucun fichier trouvé');
        }

        preg_match('/[^\/]+$/', $path, $matches);
        $filename = $matches[0];

        $response = new BinaryFileResponse($path);

        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filename
        );

        return $response;
    }
}
