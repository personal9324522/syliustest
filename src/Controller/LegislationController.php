<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LegislationController extends AbstractController
{
    public function indexAction(): Response
    {
        return $this->render('App/Legislation/index.html.twig', [
            'controller_name' => 'LegislationController',
        ]);
    }
}
