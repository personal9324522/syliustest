<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Channel\ChannelPricing;
use App\Entity\Product\ProductOptionValue;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Product\Generator\ProductVariantGenerator;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductController extends ResourceController
{

    public function createAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::CREATE);
        /** @var ProductInterface $product */
        $product = $this->newResourceFactory->create($configuration, $this->factory);

        $form = $this->resourceFormFactory->create($configuration, $product);
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::CREATE, $configuration, $product);

            if ($event->isStopped() && !$configuration->isHtmlRequest()) {
                throw new HttpException($event->getErrorCode(), $event->getMessage());
            }
            if ($event->isStopped()) {
                $this->flashHelper->addFlashFromEvent($configuration, $event);

                $eventResponse = $event->getResponse();
                if (null !== $eventResponse) {
                    return $eventResponse;
                }

                return $this->redirectHandler->redirectToIndex($configuration, $product);
            }

            if ($configuration->hasStateMachine()) {
                $stateMachine = $this->getStateMachine();
                $stateMachine->apply($configuration, $product);
            }

            // récupération du service de génération des variantes
            /** @var ProductVariantGenerator $productVariantGenerator */
            $productVariantGenerator = $this->get('sylius.generator.product_variant');
            if ($product->getOptions()->count() > 0) {
                // génération des variantes
                $productVariantGenerator->generate($product);
            }


            foreach ($product->getVariants() as $key => $variant) {
                // récupération du code du variant
                $code = $product->getCode() . '_' . $key;
                $variant->setCode($code);
                $variant->setTaxCategory($product->getTaxCategory());
                // TODO gérer le poids du variant et du product
                // récupération du channel des prix
                $channelPricing = new ChannelPricing();
                // récupération du prix de base du produit
                $price = $product->getPriceTTC();
                // récupération des montants des options
                $montantOptions = 0;
                /** @var ProductOptionValue $optionValue */
                foreach ($variant->getOptionValues() as $optionValue) {
                    $montantOptions += $optionValue->getPrice() ?? 0;
                }
                // calcul du prix total de la variante
                $price += $montantOptions;
                $channelPricing->setPrice((int)$price);
                $channelPricing->setOriginalPrice((int)$price);
                $channelPricing->setAcompte((int)$product->getAcompte());
                $channelPricing->setChannelCode($product->getChannels()->toArray()[0]->getCode());
                // ajout du channelPricing à la variante
                $variant->addChannelPricing($channelPricing);
            }
            $this->repository->add($product);


            if ($configuration->isHtmlRequest()) {
                $this->flashHelper->addSuccessFlash($configuration, ResourceActions::CREATE, $product);
            }

            $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::CREATE, $configuration, $product);

            if (!$configuration->isHtmlRequest()) {
                return $this->createRestView($configuration, $product, Response::HTTP_CREATED);
            }

            $postEventResponse = $postEvent->getResponse();
            if (null !== $postEventResponse) {
                return $postEventResponse;
            }

            return $this->redirectHandler->redirectToResource($configuration, $product);
        }
        if ($request->isMethod('POST') && $form->isSubmitted() && !$form->isValid()) {
            $responseCode = Response::HTTP_UNPROCESSABLE_ENTITY;
        }

        if (!$configuration->isHtmlRequest()) {
            return $this->createRestView($configuration, $form, Response::HTTP_BAD_REQUEST);
        }

        $initializeEvent = $this->eventDispatcher->dispatchInitializeEvent(ResourceActions::CREATE, $configuration, $product);
        $initializeEventResponse = $initializeEvent->getResponse();
        if (null !== $initializeEventResponse) {
            return $initializeEventResponse;
        }

        return $this->render($configuration->getTemplate(ResourceActions::CREATE . '.html'), [
            'configuration' => $configuration,
            'metadata' => $this->metadata,
            'resource' => $product,
            $this->metadata->getName() => $product,
            'form' => $form->createView(),
        ], null, $responseCode ?? Response::HTTP_OK);
    }
    //TODO gérer la mise à jour des prix du product dans les channel des prix des variantes
}
