<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CMS\Article;
use App\Repository\ArticleRepository;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ArticleController extends ResourceController
{
    public function listAction(): Response
    {
        /** @var ArticleRepository $repository */
        $repository = $this->get('app.repository.article');
        // récupération de tous les éléments de la table HomePage
        $articles = $repository->findBy([], ['position' => 'ASC']);

        return $this->render('App/Article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    public function detailAction(Request $request, ArticleRepository $articleRepository): Response
    {
        // récupération de l'article
        /** @var Article $article */
        $article = $this->repository->find($request->get('id'));

        return $this->render('App/Article/page.html.twig', [
            'article' => $article,
        ]);
    }

    public function updatePositionsAction(Request $request): JsonResponse
    {
        /** @var mixed $articles */
        $articles = $this->getParameterFromRequest($request, 'positions');
        // mise à jour des positions
        /** @var array $articleData */
        foreach ($articles as $articleData) {
            $article = $this->repository->find($articleData['id']);
            /** @var ?Article $article */
            if ($article instanceof Article) {
                $article->setPosition((int) $articleData['position']);
                $this->manager->persist($article);
                $this->manager->flush();
            }
        }

        return new JsonResponse();
    }

    private function getParameterFromRequest(Request $request, string $key) : mixed
    {
        if ($request !== $result = $request->attributes->get($key, $request)) {
            return $result;
        }

        if ($request->query->has($key)) {
            return $request->query->all()[$key];
        }

        if ($request->request->has($key)) {
            return $request->request->all()[$key];
        }

        return null;
    }
}
