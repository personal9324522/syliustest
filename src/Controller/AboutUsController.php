<?php

declare(strict_types=1);

namespace App\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Response;

class AboutUsController extends ResourceController
{
    public function pageAction(): Response
    {
        // récupération de tous les éléments de la table AboutUs
        $aboutus = $this->repository->findAll();

        if (empty($aboutus)) {
            return $this->redirectToRoute('app_shop_homepage_index');
        }

        return $this->render('App/AboutUs/index.html.twig', [
            'aboutus' => $aboutus[0],
        ]);
    }
}
