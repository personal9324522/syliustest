<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CMS\ConditionsGeneralesVente;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SectionController extends ResourceController
{
    public function indexAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $title = $request->attributes->get('title');


        $this->isGrantedOr403($configuration, ResourceActions::INDEX);
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);


        $event = $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $resources);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($configuration->getTemplate(ResourceActions::INDEX . '.html'), [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'sections' => $resources,
                'resources' => $resources,
                $this->metadata->getPluralName() => $resources,
                'title' => $title,
            ]);
        }


        return $this->createRestView($configuration, $resources);
    }

    public function updatePositionsAction(Request $request): JsonResponse
    {
        /** @var mixed $cgvs */
        $sections = $this->getParameterFromRequest($request, 'positions');
        // mise à jour des positions
        /** @var array $sections */
        foreach ($sections as $sectionData) {
            $section = $this->repository->find($sectionData['id']);
            if ($section instanceof ConditionsGeneralesVente) {
                $section->setPosition((int) $sectionData['position']);
                $this->manager->persist($section);
                $this->manager->flush();
            }
        }

        return new JsonResponse();
    }

    private function getParameterFromRequest(Request $request, string $key) : mixed
    {
        if ($request !== $result = $request->attributes->get($key, $request)) {
            return $result;
        }

        if ($request->query->has($key)) {
            return $request->query->all()[$key];
        }

        if ($request->request->has($key)) {
            return $request->request->all()[$key];
        }

        return null;
    }
}
