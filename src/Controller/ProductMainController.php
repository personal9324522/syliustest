<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\SectionEnum;
use App\Repository\FrequentAskQuestionRepository;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ProductMainController extends AbstractController
{
//    #[Route('/{_locale}/product/main/{category}', name: 'app_product_main_index')]
    public function indexAction(ContainerInterface $container, ?string $category): Response
    {
        // récupération du repository des questions fréquentes
        /** @var FrequentAskQuestionRepository $repository */
        $repository = $container->get('app.repository.faq');
        // récupération des questions liées à la boutique
        $faqs = $repository->findBy(['section' => SectionEnum::FAQ_SHOP],[ 'position'=> 'ASC']);

        return $this->render('@SyliusShop/Product/main.html.twig', [
            'faqs' => $faqs,
            'category' => $category,
        ]);
    }
}
