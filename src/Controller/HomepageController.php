<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\MediaEnum;
use App\Repository\ArticleRepository;
use App\Repository\HomePageRepository;
use App\Repository\MediaRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

final class HomepageController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function indexAction(HomePageRepository $homeRepository, ContainerInterface $container): Response
    {
        // récupération de tous les éléments de la table HomePage
        $homepage = $homeRepository->findAll()[0];
        // récupération du repository des medias
        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $container->get('app.repository.media');
        // récupération des questions liées à la boutique
        $medias = $mediaRepository->findBy(['type' => MediaEnum::VIDEO], null, 3);
        // récupération du repository des articles
        /** @var ArticleRepository $articleRepository */
        $articleRepository = $container->get('app.repository.article');
        // récupération des articles liés à la boutique
        $articles = $articleRepository->findBy([], null, 3);

        return new Response($this->twig->render('@SyliusShop/Homepage/index.html.twig', [
            'homepage' => $homepage,
            'medias' => $medias,
            'articles' => $articles,
        ]));
    }

    public function customAction(): Response
    {
        return new Response($this->twig->render('custom.html.twig'));
    }
}
