<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Order\OrderItem;

class OrderItemFactory
{
    public function hydrateOrderItem(OrderItem $orderItem, int $acompte): void
    {
        $orderItem->setAcompte($acompte);
    }
}
