<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\Channel\ChannelPricing;
use Sylius\Bundle\MoneyBundle\Formatter\MoneyFormatterInterface;
use Sylius\Component\Core\Exception\MissingChannelConfigurationException;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Webmozart\Assert\Assert;

class PriceFormatExtension extends AbstractExtension
{

    public function __construct(private MoneyFormatterInterface $moneyFormatter)
    {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
            new TwigFilter('getAcompte', [$this, 'getAcompte']),
        ];
    }

    public function formatPrice(int $number, string $currencyCode, string $localeCode, int $decimals = 2, string $decPoint = '.',  string $thousandsSep = ','): string
    {
        return $this->moneyFormatter->format((int)$number, $currencyCode, $localeCode);
    }

    public function getAcompte(ProductVariantInterface $productVariant, array $context): int
    {
        Assert::keyExists($context, 'channel');
        /** @var ChannelPricing $channelPricing */
        $channelPricing = $productVariant->getChannelPricingForChannel($context['channel']);

        if (null === $channelPricing || $channelPricing->getAcompte() === null) {
            throw MissingChannelConfigurationException::createForProductVariantChannelPricing($productVariant, $context['channel']);
        }

        return $channelPricing->getAcompte();
    }
}
