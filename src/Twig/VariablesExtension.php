<?php

declare(strict_types=1);

namespace App\Twig;

use App\Enum\Mails\VariableEnum;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class VariablesExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('get_variables', [$this, 'getVariables']),
        ];
    }

    public function getVariables(): bool|string
    {
        return json_encode(VariableEnum::VARIABLES_TRADUCTIONS);
    }
}
