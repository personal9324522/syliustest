<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FileExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('file_exists', [$this, 'existFile']),
            new TwigFunction('file_extract_name', [$this, 'getFileName']),
        ];
    }

    public function existFile(string $path): bool
    {
        return file_exists($path);
    }

    public function getFileName(string $fileName): string
    {
        if (preg_match('/[^\/]+$/', $fileName, $matches)) {
            return  $matches[0];
        }

        return $fileName;
    }
}
