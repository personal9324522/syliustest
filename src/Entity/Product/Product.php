<?php

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Mails\Mail;
use App\Entity\Taxation\TaxCategory;
use App\Enum\DisponibilityEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Product\Model\ProductTranslationInterface;
use Sylius\Component\Taxation\Model\TaxCategoryInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_product')]
class Product extends BaseProduct
{
    protected $variantSelectionMethod = self::VARIANT_SELECTION_MATCH;

    #[ManyToOne(targetEntity: TaxCategory::class)]
    #[JoinColumn(name: 'tax_id', referencedColumnName: 'id')]
    private ?TaxCategoryInterface $taxCategory = null;

    #[ORM\Column(type: Types::DECIMAL, nullable: true)]
    private ?float $priceHT = null;

    #[ORM\Column(type: Types::DECIMAL, nullable: true)]
    private ?float $priceTTC = null;

    #[ORM\Column(type: Types::DECIMAL, nullable: true)]
    private ?float $acompte = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $inventory = null;

    #[ORM\Column(type:Types::STRING, nullable: true)]
    private ?string $disponibility =  null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $mandatoryDeclaration = false;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $disponibilityMonth = null;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $disponibilityYear = null;

    #[ManyToOne(targetEntity: Mail::class)]
    #[JoinColumn(name: 'id_confirm_mail', referencedColumnName: 'mail_id')]
    private ?Mail $confirmEmail = null;

    #[ManyToOne(targetEntity: Mail::class)]
    #[JoinColumn(name: 'id_canceled_mail', referencedColumnName: 'mail_id')]
    private ?Mail $canceledEmail = null;

    public function getTaxCategory(): ?TaxCategoryInterface
    {
        return $this->taxCategory;
    }

    public function setTaxCategory(?TaxCategoryInterface $taxCategory): void
    {
        $this->taxCategory = $taxCategory;
    }

    public function getPriceHT(): ?float
    {
        return $this->priceHT;
    }

    public function setPriceHT(?float $priceHT): void
    {
        $this->priceHT = $priceHT;
    }

    public function getPriceTTC(): ?float
    {
        return $this->priceTTC;
    }

    public function setPriceTTC(?float $priceTTC): void
    {
        $this->priceTTC = $priceTTC;
    }

    public function getAcompte(): ?float
    {
        return $this->acompte;
    }

    public function setAcompte(?float $acompte): void
    {
        $this->acompte = $acompte;
    }

    public function getInventory(): ?int
    {
        return $this->inventory;
    }

    public function setInventory(?int $inventory): void
    {
        $this->inventory = $inventory;
    }

    public function getCurrentLocale(): string
    {
        return $this->currentLocale;
    }

    public function setOptions(Collection $options): void
    {
        $this->options = new ArrayCollection();

        foreach ($options as $option) {
            $this->addOption($option);
        }
    }

    public function getDisponibility(): ?string
    {
        return $this->disponibility;
    }

    public function setDisponibility(?string $disponibility): void
    {
        $this->disponibility = $disponibility;
    }

    public function getDisponibilityMonth(): ?string
    {
        return $this->disponibilityMonth;
    }

    public function setDisponibilityMonth(?string $disponibilityMonth): void
    {
        $this->disponibilityMonth = $disponibilityMonth;
    }

    public function getDisponibilityYear(): ?string
    {
        return $this->disponibilityYear;
    }

    public function setDisponibilityYear(?string $disponibilityYear): void
    {
        $this->disponibilityYear = $disponibilityYear;
    }

    public function isDisponible(): bool
    {
        return $this->disponibility === DisponibilityEnum::DISPO;
    }

    public function isDisponibleSoon(): bool
    {
        return $this->disponibility === DisponibilityEnum::PRE_RESERVATION;
    }

    public function isMandatoryDeclaration(): ?bool
    {
        return $this->mandatoryDeclaration;
    }

    public function setMandatoryDeclaration(?bool $mandatoryDeclaration): void
    {
        $this->mandatoryDeclaration = $mandatoryDeclaration;
    }

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    public function getMetaDescription(): ?string
    {
        return parent::getMetaDescription();
    }

    public function getConfirmEmail(): ?Mail
    {
        return $this->confirmEmail;
    }

    public function setConfirmEmail(?Mail $confirmEmail): void
    {
        $this->confirmEmail = $confirmEmail;
    }

    public function getCanceledEmail(): ?Mail
    {
        return $this->canceledEmail;
    }

    public function setCanceledEmail(?Mail $canceledEmail): void
    {
        $this->canceledEmail = $canceledEmail;
    }
}
