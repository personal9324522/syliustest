<?php

namespace App\Entity\CMS;

use App\Repository\MediaRepository;
use Sylius\Component\Resource\Model\ResourceInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[ORM\Table(name: 'cms_media')]
#[ORM\Entity(repositoryClass: MediaRepository::class)]
class Media implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'media_id', type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(name: 'media_title', length: 55, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(name: 'media_description', length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(name: 'media_path', length: 255, nullable: true)]
    private ?string $path = null;

    #[ORM\Column(name: 'media_type', length: 50, nullable: true)]
    private ?string $type = null;

    private ?UploadedFile $uploadedFile = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): void
    {
        $this->path = $path;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getUploadedFile(): ?UploadedFile
    {
        return $this->uploadedFile;
    }

    public function setUploadedFile(?UploadedFile $uploadedFile): void
    {
        $this->uploadedFile = $uploadedFile;
    }
}
