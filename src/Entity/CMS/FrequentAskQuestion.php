<?php

namespace App\Entity\CMS;

use App\Repository\FrequentAskQuestionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;


#[ORM\Table(name: 'cms_faq')]
#[ORM\Entity(repositoryClass: FrequentAskQuestionRepository::class)]
class FrequentAskQuestion implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'faq_id', type: 'integer')]
    private int $id;

    #[ORM\Column(name: 'faq_question', length: 255, nullable: true)]
    private ?string $question = null;

    #[ORM\Column(name: 'faq_answer',type: Types::TEXT, nullable: true)]
    private ?string $answer = null;

    #[ORM\Column(name: 'faq_section', length: 50, nullable: true)]
    private ?string $section = null;

    #[ORM\Column(name: 'faq_position', type: 'integer', nullable: true)]
    private ?int $position = null;

    public function getId(): int
    {
        return $this->id;
    }
    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): void
    {
        $this->question = $question;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): void
    {
        $this->answer = $answer;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): void
    {
        $this->section = $section;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }
}
