<?php

declare(strict_types=1);

namespace App\Entity\CMS;

use App\Repository\MentionsLegalesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'cms_cc')]
#[ORM\Entity(repositoryClass: MentionsLegalesRepository::class)]
class CharteConfidentialite extends Section
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'cc_id', type: 'integer')]
    protected ?int $id = null;

    #[ORM\Column(name: 'cc_title', length: 55, nullable: true)]
    protected ?string $title = null;

    #[ORM\Column(name: 'cc_html', type: Types::TEXT, nullable: true)]
    protected ?string $content = null;

    #[ORM\Column(name: 'cc_position', type: 'integer', nullable: true)]
    protected ?int $position = null;
}
