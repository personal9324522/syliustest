<?php

declare(strict_types=1);

namespace App\Entity\CMS;

use App\Repository\ConditionsGeneralesVenteRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'cms_cgv')]
#[ORM\Entity(repositoryClass: ConditionsGeneralesVenteRepository::class)]
class ConditionsGeneralesVente extends Section
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'cgv_id', type: 'integer')]
    protected ?int $id = null;

    #[ORM\Column(name: 'section_cgv_title', length: 55, nullable: true)]
    protected ?string $title = null;

    #[ORM\Column(name: 'section_cgv_html', type: Types::TEXT, nullable: true)]
    protected ?string $content = null;

    #[ORM\Column(name: 'section_cgv_position', type: 'integer', nullable: true)]
    protected ?int $position = null;

}
