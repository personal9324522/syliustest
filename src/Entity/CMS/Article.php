<?php

declare(strict_types=1);

namespace App\Entity\CMS;

use App\Enum\SectionEnum;
use App\Repository\ArticleRepository;
use Doctrine\DBAL\Types\Types;
use Sylius\Component\Resource\Model\ResourceInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'cms_article')]
#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'article_id', type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(name: 'article_title', length: 55, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(name: 'article_html', type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    #[ORM\Column(name: 'article_section', length: 50, nullable: true)]
    private ?string $section = SectionEnum::ARTICLE;

    #[ORM\Column(name: 'article_position', type: 'integer', nullable: true)]
    private ?int $position = null;

    #[ORM\Column(name: 'article_imageurl', type: 'string', length: 255, nullable: true)]
    private ?string $imageUrl = null;

    #[ORM\Column(name: 'article_tags', length: 255, nullable: true)]
    private ?string $metaTags = null;

    #[ORM\Column(name: 'article_meta_description', type: Types::STRING, nullable: true)]
    private ?string $metaDescription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): void
    {
        $this->section = $section;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    public function getMetaTags(): ?string
    {
        return $this->metaTags;
    }

    public function setMetaTags(?string $metaTags): void
    {
        $this->metaTags = $metaTags;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
