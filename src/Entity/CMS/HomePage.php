<?php

namespace App\Entity\CMS;

use App\Repository\HomePageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

#[ORM\Table(name: 'cms_homepage')]
#[ORM\Entity(repositoryClass: HomePageRepository::class)]
class HomePage implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'homepage_id', type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(name: 'homepage_title', length: 55, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(name: 'homepage_maindescription', length: 255, nullable: true)]
    private ?string $mainDescription = null;

    #[ORM\Column(name: 'homepage_about', length: 400, nullable: true)]
    private ?string $about = null;

    #[ORM\Column(name: 'homepage_tags', length: 255, nullable: true)]
    private ?string $metaTags = null;

    #[ORM\Column(name: 'homepage_meta_description', type: Types::STRING, nullable: true)]
    private ?string $metaDescription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getMainDescription(): ?string
    {
        return $this->mainDescription;
    }

    public function setMainDescription(?string $mainDescription): static
    {
        $this->mainDescription = $mainDescription;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): static
    {
        $this->about = $about;

        return $this;
    }

    public function getMetaTags(): ?string
    {
        return $this->metaTags;
    }

    public function setMetaTags(?string $metaTags): void
    {
        $this->metaTags = $metaTags;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
