<?php

declare(strict_types=1);

namespace App\Entity\CMS;

use App\Repository\MentionsLegalesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'cms_ml')]
#[ORM\Entity(repositoryClass: MentionsLegalesRepository::class)]
class MentionsLegales extends Section
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'ml_id', type: 'integer')]
    protected ?int $id = null;

    #[ORM\Column(name: 'ml_title', length: 55, nullable: true)]
    protected ?string $title = null;

    #[ORM\Column(name: 'ml_html', type: Types::TEXT, nullable: true)]
    protected ?string $content = null;

    #[ORM\Column(name: 'ml_position', type: 'integer', nullable: true)]
    protected ?int $position = null;
}
