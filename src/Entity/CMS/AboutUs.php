<?php

namespace App\Entity\CMS;

use App\Enum\SectionEnum;
use App\Repository\MediaRepository;
use Doctrine\DBAL\Types\Types;
use Sylius\Component\Resource\Model\ResourceInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'cms_about')]
#[ORM\Entity(repositoryClass: MediaRepository::class)]
class AboutUs implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'about_id', type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(name: 'about_title', length: 55, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(name: 'about_description', type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(name: 'about_section', length: 50, nullable: true)]
    private ?string $section = SectionEnum::ABOUT;

    #[ORM\Column(name: 'about_tags', length: 255, nullable: true)]
    private ?string $metaTags = null;

    #[ORM\Column(name: 'about_meta_description', type: Types::STRING, nullable: true)]
    private ?string $metaDescription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): void
    {
        $this->section = $section;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): void
    {
        $this->tags = $tags;
    }

    public function getMetaTags(): ?string
    {
        return $this->metaTags;
    }

    public function setMetaTags(?string $metaTags): void
    {
        $this->metaTags = $metaTags;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
