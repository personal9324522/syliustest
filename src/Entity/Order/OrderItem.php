<?php

declare(strict_types=1);

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\OrderItem as BaseOrderItem;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order_item")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_order_item')]
class OrderItem extends BaseOrderItem
{
    #[ORM\Column(type: 'integer', nullable: false)]
    private ?int $acompte = null;

    private ?int $totalAcompte = null;

    private ?int $totalRestant = null;

    public function getAcompte(): ?int
    {
        return $this->acompte;
    }

    public function setAcompte(?int $acompte): void
    {
        $this->acompte = $acompte;
    }
}
