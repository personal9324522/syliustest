<?php

declare(strict_types=1);

namespace App\Entity\Order;

use App\Entity\Payment\Payment;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Order as BaseOrder;
use Sylius\Component\Core\OrderPaymentStates;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_order')]
class Order extends BaseOrder
{
    public const CONFIRMED = 'confirmed';

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $itemsTotalAcompte = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $totalAcompte;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $totalRestant;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $itemsSubtotalAcompte = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $isTotallyPaid = false;


    public function getItemsTotalAcompte(): ?int
    {
        return $this->itemsTotalAcompte;
    }

    public function setItemsTotalAcompte(?int $itemsTotalAcompte): void
    {
        $this->itemsTotalAcompte = $itemsTotalAcompte;
    }

    public function getTotalAcompte(): ?int
    {
        return $this->totalAcompte;
    }

    public function setTotalAcompte(?int $totalAcompte): void
    {
        $this->totalAcompte = $totalAcompte;
    }

    public function getTotalRestant(): ?int
    {
        return $this->totalRestant;
    }

    public function setTotalRestant(?int $totalRestant): void
    {
        $this->totalRestant = $totalRestant;
    }

    public function getItemsSubtotalAcompte(): ?int
    {
        return $this->itemsSubtotalAcompte;
    }

    public function setItemsSubtotalAcompte(?int $itemsSubtotalAcompte): void
    {
        $this->itemsSubtotalAcompte = $itemsSubtotalAcompte;
    }

    public function isCartState(): bool
    {
        return $this->state === self::STATE_CART;
    }

    public function isPartiallyPaid(): bool
    {
        return $this->paymentState === OrderPaymentStates::STATE_PARTIALLY_PAID;
    }

    public function isTotallyPaid(): ?bool
    {
        return $this->isTotallyPaid;
    }

    public function setIsTotallyPaid(?bool $isTotallyPaid): void
    {
        $this->isTotallyPaid = $isTotallyPaid;
    }
}
