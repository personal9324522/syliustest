<?php

declare(strict_types=1);

namespace App\Entity\Util;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class OptionCombination
{
    public Collection $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function isSameCombination(Collection $compareOptions): bool
    {
        $diff = array_diff($compareOptions->toArray(), $this->options->toArray());
        if (!empty($diff)) {
            return false;
        }

        return true;
    }
}
