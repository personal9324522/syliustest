<?php

declare(strict_types=1);

namespace App\Entity\Mails;

use App\Repository\MailRepository;
use Doctrine\DBAL\Types\Types;
use Sylius\Component\Resource\Model\ResourceInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'mails_mail')]
#[ORM\Entity(repositoryClass: MailRepository::class)]
class Mail implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'mail_id', type: Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(name: 'mail_name', type: Types::STRING, length: 55, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(name: 'mail_object', type: Types::STRING, length: 100, nullable: true)]
    private ?string $object = null;

    #[ORM\Column(name: 'mail_content', type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    #[ORM\Column(name: 'mail_files', type: Types::SIMPLE_ARRAY, nullable: true)]
    private array $files = [];

    private array $uploadedFiles = [];

    public function __toString(): string
    {
        return $this->name ?? '';
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(?string $object): void
    {
        $this->object = $object;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    public function addFile(string $fileName): void
    {
        $this->files[] = $fileName;
    }

    public function removeFile(string $fileName): void
    {
        $updatedFile = [];
        foreach ($this->files as $file) {
            if ($fileName !== $file) {
                $updatedFile[] = $file;
            }
        }

        $this->setFiles($updatedFile);
    }

    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    public function setUploadedFiles(array $uploadedFiles): void
    {
        $this->uploadedFiles = $uploadedFiles;
    }
}
