<?php

namespace App\Entity\Mails;

use App\Repository\SignatureRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

#[ORM\Table(name: 'mails_signature')]
#[ORM\Entity(repositoryClass: SignatureRepository::class)]
class Signature implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'signature_id', type: Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(name: 'signature_content', type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    public function getId()
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
}
