<?php

declare(strict_types=1);

namespace App\Entity\Channel;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ChannelPricing as BaseChannelPricing;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel_pricing")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_channel_pricing')]
class ChannelPricing extends BaseChannelPricing
{
    #[ORM\Column(type: 'integer', nullable: false)]
    private ?int $acompte = null;

    public function getAcompte(): ?int
    {
        return $this->acompte;
    }

    public function setAcompte(?int $acompte): void
    {
        $this->acompte = $acompte;
    }
}
