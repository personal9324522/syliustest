<?php

declare(strict_types=1);

namespace App\Menu;

use Sylius\Bundle\AdminBundle\Event\ProductMenuBuilderEvent;

final class AdminProductFormMenuListener
{
    public function addItems(ProductMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menu
            ->removeChild('associations')
        ;
        $menu
            ->addChild('pricing')
            ->setLabel('Prix')
            ->setAttribute('template', 'bundles/SyliusAdminBundle/Product/Tab/_pricing.html.twig')
        ;
        $menu
            ->addChild('taxes')
            ->setLabel('Taxe')
            ->setAttribute('template', 'bundles/SyliusAdminBundle/Product/Tab/_taxes.html.twig')
        ;
        $menu
            ->addChild('inventory')
            ->setLabel('Stock')
            ->setAttribute('template', 'bundles/SyliusAdminBundle/Product/Tab/_inventory.html.twig')
        ;
        $menu
            ->addChild('emails')
            ->setLabel('E-mails')
            ->setAttribute('template', 'bundles/SyliusAdminBundle/Product/Tab/_emails.html.twig')
        ;
    }
}
