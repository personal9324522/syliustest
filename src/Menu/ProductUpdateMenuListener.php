<?php

declare(strict_types=1);

namespace App\Menu;

use Sylius\Bundle\AdminBundle\Event\ProductMenuBuilderEvent;

class ProductUpdateMenuListener
{
    public function removeItems(ProductMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();
        // récupération de l'item "manage_variants"
        $manageVariantsItem = $menu->getChild('manage_variants');
        // suppression des items de création
        $manageVariantsItem->removeChild('product_variant_create');
        $manageVariantsItem->removeChild('product_variant_generate');
    }
}
