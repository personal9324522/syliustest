<?php

declare(strict_types=1);

namespace App\Menu;

use App\Entity\Order\Order;
use Sylius\Bundle\AdminBundle\Event\OrderShowMenuBuilderEvent;
use Sylius\Component\Core\Model\OrderInterface;

final class AdminOrderShowMenuListener
{
    public function addAdminOrderShowMenuItems(OrderShowMenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();
        $order = $event->getOrder();

        if (null !== $order->getId() && $order->getState() === OrderInterface::STATE_NEW) {
            $menu
                ->addChild('confirm', [
                    'route' => 'app_admin_order_confirm',
                    'routeParameters' => ['id' => $order->getId()]
                ])
                ->setAttribute('type', 'transition')
                ->setLabel('Confirmer la réservation')
                ->setLabelAttribute('icon', 'checkmark')
                ->setLabelAttribute('color', 'green')
            ;
        }
    }
}
