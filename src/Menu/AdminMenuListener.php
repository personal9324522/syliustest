<?php

declare(strict_types=1);

namespace App\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $menu->getChild('catalog')->removeChild('association_types');
        $menu->removeChild('marketing');

        $content = $menu
            ->addChild('content', [])
            ->setLabel('Contenu')
        ;

        $content
            ->addChild('homepage', [
                'route' => 'app_admin_homepage_update',
                'routeParameters' => [ 'id' => 1 ]
            ])
            ->setLabel('Page d\'accueil')
            ;

        $content
            ->addChild('articles', ['route' => 'app_admin_article_index'])
            ->setLabel('Articles')
            ;

        $content
            ->addChild('aboutus', [
                'route' => 'app_admin_aboutus_update',
                'routeParameters' => [ 'id' => 1 ]
            ])
            ->setLabel('A propos')
            ;

        $content
            ->addChild('faq', ['route' => 'app_admin_faq_index'])
            ->setLabel('FAQ')
            ;

        $content
            ->addChild('media', ['route' => 'app_admin_media_index'])
            ->setLabel('Médias')
            ;

        $content
            ->addChild('cgv', ['route' => 'app_admin_cgv_index'])
            ->setLabel('CGV')
            ;

        $content
            ->addChild('cc', ['route' => 'app_admin_cc_index'])
            ->setLabel('Confidentialité')
            ;

        $content
            ->addChild('ml', ['route' => 'app_admin_ml_index'])
            ->setLabel('Mentions légales')
            ;

        $contentMails = $menu
            ->addChild('mails', [])
            ->setLabel('E-mails')
        ;

        $contentMails
            ->addChild('mail', ['route' => 'app_admin_mail_index'])
            ->setLabel('Modèles')
        ;

        $contentMails
            ->addChild('signature', [
                'route' => 'app_admin_signature_update',
                'routeParameters' => [ 'id' => 1 ]
            ])
            ->setLabel('Signature')
        ;

        $menu->reorderChildren([
            1 => 'catalog',
            2 => 'sales',
            3 => 'customers',
            4 => 'mails',
            5 => 'configuration',
            6 => 'content',
        ]);
    }
}
