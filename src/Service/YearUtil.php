<?php

declare(strict_types=1);

namespace App\Service;

class YearUtil
{
    public static function getYearsChoices(): array
    {
        $years = [];
        $currentYear = (int) date('Y');
        $years[(string) $currentYear] = (string) $currentYear;
        for ($i = 1; $i <= 5; ++$i) {
            $currentYear ++;
            $years[(string)$currentYear] = (string) $currentYear;
        }

        return $years;
    }
}
