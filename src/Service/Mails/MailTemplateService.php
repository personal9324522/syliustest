<?php

declare(strict_types=1);

namespace App\Service\Mails;

use App\Entity\Customer\Customer;
use App\Entity\Mails\Mail;
use App\Entity\Order\Order;
use App\Enum\Mails\VariableEnum;

final class MailTemplateService
{
    const VARIABLE_REGEX = "/\[\[([A-Za-z ]+)\]\]/";

    final public function getMailContent(Order $order, Customer $customer, Mail $mail): string
    {
        // récupération du contenu du mail
        $content = $mail->getContent();
        $variablesValues = $this->getVariablesValue($order, $content);

        if (!empty($variablesValues)) {
            // remplacement des variables par leur valeur
            $content = $this->repaceVariablesValu($variablesValues, $content);
        }

        return $content;
    }

    private function getVariablesValue(Order $order, string $content): array
    {
        $values = [];
        preg_match_all(self::VARIABLE_REGEX,$content , $matches);
        if (count($matches) > 0) {
            // récupération des variables
            $variables = array_unique($matches[1]);
            foreach ($variables as $variable) {
                // récupération de la fonction liée à la variable
                $function = 'get'.ucfirst(VariableEnum::getAttributeName($variable));
                $object = $this->getPathObjectFunction($order, $variable);
                if(method_exists($object::class, $function)) {
                    $value = $object->$function();
                    $values[$variable] = $value;                }

            }
        }

        return $values;
    }

    private function repaceVariablesValu(array $variablesValue, string $content): string
    {
        // remplace les variables par leur valeur
        foreach ($variablesValue as $variable => $value) {
            $content = str_replace("[[$variable]]", $value, $content);
        }

        return $content;
    }

    private function getPathObjectFunction(Order $order, string $variable): mixed
    {
        return match (true) {
            in_array($variable, VariableEnum::VARIABLES_CUSTOMER) => $order->getCustomer(),
            in_array($variable, VariableEnum::VARIABLES_PRODUCT) => $order->getItems()->first()->getVariant()->getProduct(),
            default =>  $order,
        };
    }
}
