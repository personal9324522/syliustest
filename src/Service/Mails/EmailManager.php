<?php

namespace App\Service\Mails;

use App\Entity\Mails\Mail;
use App\Entity\Order\Order;
use App\Entity\Order\OrderItem;
use App\Entity\Product\Product;
use App\Repository\MailRepository;
use App\Repository\SignatureRepository;
use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\BodyRendererInterface;

final class EmailManager
{
    const CONFIRM_ORDER = 'confirm_order';
    const CANCEL_ORDER = 'cancel_order';
    const ORDER = 'order';

    public function __construct(
        private MailerInterface $mailer,
        private MailTemplateService $mailTemplateService,
        private MailRepository $mailRepository,
        private SignatureRepository $signatureRepository,
        private BodyRendererInterface $bodyRenderer,
        private string $env,
    ) {
    }

    public function sendConfirmOrderEmail(OrderInterface $order): void
    {
       $this->sendOrderMail($order, self::CONFIRM_ORDER);
    }

    public function sendCancelOrderEmail(Order $order): void
    {
        $this->sendOrderMail($order, self::CANCEL_ORDER);
    }

    private function sendOrderMail(OrderInterface $order, string $type): void
    {
        if ($order instanceof Order) {
            // initialisation du tableau des fichiers à envoyer
            $files = [];
            // initialisation du tableau des mails
            $mails = [];
            // récupération du mail du client
            $customer= $order->getCustomer();
            // récupération des products
            $products = array_map(function (OrderItem $orderItem) {
                return $orderItem->getProduct();
            }, $order->getItems()->toArray());
            // récupération des fichiers
            /** @var Product $product **/
            foreach ($products as $product) {
                // récupération du mail
                $email = match($type){
                    self::CANCEL_ORDER => $product->getConfirmEmail(),
                    self::CONFIRM_ORDER => $product->getCanceledEmail(),
                    default => null,
                };
                $mails[$email->getName()] = $email;
                foreach ($email->getFiles() as $file) {
                    $files[$file] = $file;
                }
            }
            if (!empty($mails)) {
                $mails = array_values($mails);
                // récupération du contenu avec les variables
                $content = $this->mailTemplateService->getMailContent($order, $customer, $mails[0]);
                // récupération du text de réservation pour l'object du mail
                $reservationText = ' Réservation '. $order->getNumber();
                // récupération de l'objet du mail
                $subject = $mails[0] instanceof Mail ? $mails[0]->getObject() . $reservationText: $reservationText;
                // récupération de l'email du client
                $customerEmail = $customer->getEmail();

                // envoi du mail
                $this->sendMail($content, $subject, $customerEmail, $files);
            }
        }
    }

    private function sendMail(string $content, string $subject, string $to, array $files): void
    {
        // récupération de la signature
        $signature = $this->signatureRepository->findAll();
        // création du mail
        $mail = new TemplatedEmail();
        $mail
            ->from('jbponzio@hotmail.fr')
            ->to($to)
            ->addCc('jbponzio@hotmail.fr')
            ->subject($subject)
            ->htmlTemplate('App/Mails/base.html.twig')
            ->context([
                'content' => $content,
                'signature' => !empty($signature) ? $signature[0]->getContent() : '',
            ]);

        // ajout des fichiers
        foreach ($files as $file) {
            $mail->attachFromPath($file);
        }

        $this->bodyRenderer->render($mail);

        if ($this->env === 'dev') {
            return;
        }

        try {
            // envoi du mail
            $this->mailer->send($mail);
        }catch (\Exception $e) {
            throw new \Exception('Le mail n\'a pas pu être envoyé');
        }
    }
}
