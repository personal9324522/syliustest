<?php

declare(strict_types=1);

namespace App\Service\Mails;

use App\Entity\Mails\Mail;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    const DIRECTORY = 'upload/files/mails/';

    final public function uploadFile(?UploadedFile $file, Mail $mail): void
    {
        // gestion du fichier null
        if (!$file instanceof UploadedFile) {

            return;
        }

        // récupération du nom du fichier
        $fileName = $file->getClientOriginalName();
        // récupération du nom complet
        $completeFileName = $this->getCompleteFileName($mail, $fileName);
        // mise à jour des fichiers du mail
        $mail->addFile($completeFileName.$fileName);

        try {
            // upload physique du fichier
            $file->move($completeFileName, $fileName);
        } catch (FileException $e) {
            // creating a error message
            throw new FileException("Le fichier ({$file->getClientOriginalName()}) n'a pas pu être téléversé. {$e->getMessage()}");
        }
    }

    final public function deleteFile(Mail $mail, string $filename): void
    {
        unlink($filename);
        $mail->removeFile($filename);
    }

    private function getCompleteFileName(Mail $mail, string $fileName): string
    {
        return self::DIRECTORY.$mail->getId().'/';
    }
}
