<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\CMS\Media;
use App\Enum\MediaEnum;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaService
{
    const VIDEO_DIRECTORY = '/upload/media/video';
    const IMAGE_DIRECTORY = '/upload/media/image';

    public function uploadMedia(?UploadedFile $file, Media $media, ?string $fileName = null): void
    {
        // gestion du fichier null
        if (!$file instanceOf UploadedFile) {

            return;
        }

        // gestion nom du fichier vide
        if (!$fileName) {
            // création du nom unique du fichier téléversé
            $fileName = uniqid('', true);
            // concaténation de l'extension du fichier
            $fileName = $fileName.'.'.$file->guessExtension();
        }

        // récupération du répertoire de destination
        $directory = $this->isImage($file) ? self::IMAGE_DIRECTORY : self::VIDEO_DIRECTORY;

        // mise à jour de l'entité
        $this->updateMedia($this->getPath($directory, $fileName), $media);

        try {
            // upload physique du fichier
            $file->move($directory, $fileName);
        } catch (FileException $e) {
            // creating a error message
            throw new FileException("Le fichier ({$file->getClientOriginalName()}) n'a pas pu être téléversé. {$e->getMessage()}");
        }
    }

    public function deleteFile(Media $media): void
    {
        // récupération du chemin du fichier
        $path = $media->getPath();
        if ($path) {
            // suppression du fichier
            unlink($path);
        }

        // mise à jour du média
        $this->updateMedia(null, $media);
    }

    private function isImage(UploadedFile $file): bool
    {
        return str_contains($file->getMimeType() ?? '', 'image');
    }

    private function updateMedia(?string $path, Media $media): void
    {
        if ($media->getUploadedFile() instanceof UploadedFile) {
            $type = $this->isImage($media->getUploadedFile()) ? MediaEnum::IMAGE : MediaEnum::VIDEO;
            $media->setType($type);
            $media->setPath($path);
        }

        if (!$path) {
            $media->setType(null);
            $media->setPath(null);
        }
    }

    private function getPath(string $directory, string $filename): string
    {
        return $directory . '/' . $filename;
    }
}
