<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order\Order;
use Sylius\Component\Core\Payment\Provider\OrderPaymentProviderInterface;
use Sylius\Component\Payment\Model\PaymentInterface;

final class OrderStateService
{
    public function __construct(
        private OrderPaymentProviderInterface $orderPaymentProvider,
        private string $targetState = PaymentInterface::STATE_CART,
        /** @var array<string> $unprocessableOrderStates */
    ) {
    }

    public function confirmOrder(Order $order): void
    {
        // création d'un nouveau paiement pour le restant à payer si la réservation n'a pas été totalement réglée
        if (!$order->isTotallyPaid()) {
            // création d'un nouveau paiement
            /** @var PaymentInterface $newPayment */
            $newPayment = $this->orderPaymentProvider->provideOrderPayment($order, $this->targetState);
            // mise à jour du montant du paiement (restant)
            $newPayment->setAmount($order->getTotalRestant() ?? 0);
            $newPayment->setState(PaymentInterface::STATE_NEW);
            // ajout du paiement à la commande
            $order->addPayment($newPayment);
        }
    }
}
