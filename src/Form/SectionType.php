<?php

declare(strict_types=1);

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class SectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Sections',
            ])
            ->add('position', IntegerType::class, [
                'label' => 'Position',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Contenu',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_section';
    }
}
