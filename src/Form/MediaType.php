<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre du média',
            ])
            ->add('uploadedFile', FileType::class, [
                'label' => 'Fichier (image ou vidéo)',
                'attr' =>
                [
                    'placeholder' => 'Choisissez un fichier',
                ]
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_media';
    }
}
