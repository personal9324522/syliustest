<?php

declare(strict_types=1);

namespace App\Form;

use Sylius\Bundle\OrderBundle\Form\Type\CartType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CartTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $event->getForm()->add('isTotallyPaid', CheckboxType::class, [
                'required' => false,
                'label' => 'Je souhaite régler la totalité de la réservation dès maintenant',
            ]);
        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [CartType::class];
    }
}
