<?php

declare(strict_types=1);

namespace App\Form;

use Sylius\Bundle\CoreBundle\Form\Type\Product\ChannelPricingType;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ChannelPricingTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            $event->getForm()
                ->add('acompte', MoneyType::class, [
                    'label' => 'Montant de l\'acompte',
                ])
                ->remove('minimumPrice')
            ;
        });
    }


    public static function getExtendedTypes(): iterable
    {
        return [ChannelPricingType::class];
    }
}
