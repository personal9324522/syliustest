<?php

declare(strict_types=1);

namespace App\Form;

use App\Enum\SectionEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FrequentAskQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('question', TextType::class, [
                'label' => 'Question',
            ])
            ->add('answer', TextareaType::class, [
                'label' => 'Réponse',
            ])
            ->add('section', ChoiceType::class, [
                'label' => 'Quelle page de FAQ',
                'choices' => [
                    'Page principale des FAQ' => SectionEnum::MAIN_FAQ,
                    'Page de la boutique' => SectionEnum::FAQ_SHOP,
                ],
            ])
            ->add('position', IntegerType::class, [
                'label' => 'Position',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_faq';
    }
}
