<?php

namespace App\Form;

use App\Entity\Mails\Mail;
use App\Entity\Product\Product;
use App\Enum\DisponibilityEnum;
use App\Enum\MonthEnum;
use App\Service\YearUtil;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Sylius\Bundle\TaxationBundle\Form\Type\TaxCategoryChoiceType;
use Sylius\Component\Core\Model\ProductInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProductTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var ProductInterface $product */
            $product = $event->getData();
            // modification du comportement d'affichage des options pour toujours activer le champ
            // récupération de la configuration du champ des options
//            $config = $event->getForm()->get('options')->getConfig();
//            $options = $config->getOptions();
//            $options['disabled'] = false;

            $event->getForm()
                ->add('priceHT', MoneyType::class, [
                    'label' => 'Montant hors taxe',
                    'required' => false,
                ])
                ->add('priceTTC', MoneyType::class, [
                    'label' => 'Montant TTC',
                    'required' => false,
                ])
                ->add('acompte', MoneyType::class, [
                    'label' => 'Montant de l\'acompte',
                    'required' => false,
                ])
                ->add('taxCategory', TaxCategoryChoiceType::class, [
                    'required' => false,
                    'placeholder' => '---',
                    'label' => 'Taxe à appliquer',
                ])
                ->add('inventory', IntegerType::class, [
                    'required' => false,
                    'label' => 'Stock',
                    'empty_data' => 0,
                ])
                ->add('disponibility', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Disponibilité',
                    'choices' => DisponibilityEnum::getAll(),
                    'placeholder' => '---',
                    'empty_data' => null,
                ])
                ->add('disponibilityMonth', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Mois de disponibilité',
                    'choices' => MonthEnum::getMonths(),
                    'placeholder' => '---',
                    'empty_data' => null,
                ])
                ->add('disponibilityYear', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Année de disponibilité',
                    'choices' => YearUtil::getYearsChoices(),
                    'placeholder' => '---',
                    'empty_data' => null,
                ])
                ->add('mandatoryDeclaration', CheckboxType::class, [
                    'required' => false,
                    'label' => 'Nécessite une déclaration obligatoire',
                ])
                ->add('canceledEmail', EntityType::class, [
                    'class' => Mail::class,
                    'required' => false,
                    'label' => 'Mail d\'annulation',
                ])
                ->add('confirmEmail', EntityType::class, [
                    'class' => Mail::class,
                    'required' => false,
                    'label' => 'Mail de confirmation de la réservation',
                ])

                ->remove('variantSelectionMethod')
            ;
        });
        $this->setDefaultDisponibilityDateValue($builder);
    }

    private function setDefaultDisponibilityDateValue(FormBuilderInterface $builder): void
    {
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            // récupération du produit
            $product = $event->getData();
            if (!$product instanceof Product) {
                return;
            }
            // récupération du mois de disponibilité
            $disponibilityMonth = in_array($product->getDisponibility(), [DisponibilityEnum::INDISPONIBLE, DisponibilityEnum::DISPO],true) ? null : $product->getDisponibilityMonth();
            $product->setDisponibilityMonth($disponibilityMonth);

            if (DisponibilityEnum::PRE_RESERVATION === $product->getDisponibility() && (null === $product->getDisponibilityMonth())) {
                // ajout d'une violation de contrainte sur le mois de disponibilité
                $event->getForm()->get('disponibilityMonth')->addError(new FormError('Le mois de disponibilité doit être renseigné si une disponibilité est prévue"'));
            }
            if (DisponibilityEnum::PRE_RESERVATION === $product->getDisponibility() && (null === $product->getDisponibilityYear())) {
                // ajout d'une violation de contrainte sur le mois de disponibilité
                $event->getForm()->get('disponibilityYear')->addError(new FormError('L\'année  de disponibilité doit être renseigné si une disponibilité est prévue"'));
            }
        });
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }
}
