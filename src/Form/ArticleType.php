<?php

declare(strict_types=1);

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre de l\'article',
            ])
            ->add('imageUrl', HiddenType::class, [
                'attr' => [
                    'data-admin--image-target' => 'inputImage',
                ],
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Contenu de l\'article',
            ])
            ->add('position', IntegerType::class, [
                'label' => 'Position',
            ])
            ->add('metaTags', TextType::class, [
                'label' => 'Meta tags',
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Meta description',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_article';
    }
}
