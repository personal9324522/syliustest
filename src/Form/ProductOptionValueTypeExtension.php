<?php

declare(strict_types=1);

namespace App\Form;

use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

class ProductOptionValueTypeExtension extends AbstractTypeExtension
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Adding new fields works just like in the parent form type.
            ->add('price', MoneyType::class, [
                'required' => false,
                'label' => 'Montant de l\'option (non obligatoire)',
            ]);
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductOptionValueType::class];
    }
}
