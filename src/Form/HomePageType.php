<?php

declare(strict_types=1);

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HomePageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre  principal',
            ])
            ->add('mainDescription', TextType::class, [
                'label' => 'Description principale',
            ])
            ->add('about', TextareaType::class, [
                'label' => 'Contenu de la section A PROPOS de la page d\'accueil',
            ])
            ->add('metaTags', TextType::class, [
                'label' => 'Meta tags',
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Meta description',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_hompage';
    }
}
