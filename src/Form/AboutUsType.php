<?php

declare(strict_types=1);

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AboutUsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Contenu',
            ])
            ->add('metaTags', TextType::class, [
                'label' => 'Meta tags',
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Meta description',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_aboutus';
    }
}
