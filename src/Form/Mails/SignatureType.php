<?php

declare(strict_types=1);

namespace App\Form\Mails;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SignatureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('content', CKEditorType::class, [
                'label' => 'Signature des mails',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_signature';
    }
}
