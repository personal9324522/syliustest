<?php

declare(strict_types=1);

namespace App\Form\Mails;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du modèle',
            ])
            ->add('object', TextType::class, [
                'label' => 'Objet du mail',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Contenu du mail',
            ])
            ->add('uploadedFiles', FileType::class, [
                'required' => false,
                'label' => 'Fichiers attachés au mail',
                'multiple' => true,
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_mail';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['data-controller' => 'admin--variables']
        ]);
    }
}
