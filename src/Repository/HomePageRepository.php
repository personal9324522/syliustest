<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CMS\HomePage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class HomePageRepository extends ServiceEntityRepository implements RepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, HomePage::class);
    }



    public function add(ResourceInterface $resource): void
    {
        // TODO: Implement add() method.
    }

    public function remove(ResourceInterface $resource): void
    {
        // TODO: Implement remove() method.
    }

    public function createPaginator(array $criteria = [], array $sorting = []): iterable
    {
        // TODO: Implement createPaginator() method
        return [];
    }
}
