<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Order\Order;
use Doctrine\ORM\NonUniqueResultException;

final class OrderRepository extends \Sylius\Bundle\CoreBundle\Doctrine\ORM\OrderRepository
{
    /**
     * @throws NonUniqueResultException
     */
    final public function findDetailOrderByIdForMail(string $id): ?Order
    {
        $qb = $this->createQueryBuilder('o');

        $qb
            ->addSelect('customer')
            ->addSelect('items')
            ->addSelect('variant')
            ->addSelect('product')
            ->addSelect('canceledEmail')
            ->addSelect('confirmEmail')
            ->leftJoin('o.customer', 'customer')
            ->leftJoin('o.items', 'items')
            ->leftJoin('items.variant', 'variant')
            ->leftJoin('variant.product', 'product')
            ->leftJoin('product.confirmEmail', 'confirmEmail')
            ->leftJoin('product.canceledEmail', 'canceledEmail')
            ->andWhere('o.id = :id')
            ->setParameter('id', $id);


        return $qb->getQuery()->getOneOrNullResult();
    }
}
