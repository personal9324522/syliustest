<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Taxonomy\Taxon;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository as BaseTaxonRepository;
use Sylius\Component\Taxonomy\Model\TaxonInterface;

class TaxonRepository extends BaseTaxonRepository
{
    public function findChildrenByChannelMenuTaxon(?TaxonInterface $menuTaxon = null, ?string $locale = null): array
    {
        $hydrationQuery = $this->createTranslationBasedQueryBuilder($locale)
            ->addSelect('o')
            ->addSelect('oc')
            ->leftJoin('o.children', 'oc')
        ;

        if (null !== $menuTaxon) {
            $hydrationQuery
                ->andWhere('o.root = :root')
                ->setParameter('root', $menuTaxon)
            ;
        }

        $hydrationQuery->getQuery()->getResult();
        $resultsQuery = $this->createTranslationBasedQueryBuilder($locale)
            ->addSelect('child')
            ->addSelect('parent')
            ->innerJoin('o.parent', 'parent')
            ->leftJoin('o.children', 'child')
            ->andWhere('o.enabled = :enabled')
            ->addOrderBy('o.position')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;

        return array_unique(array_map(function (Taxon $result) {

            return $result->getParent();
        }, $resultsQuery));
    }
}
