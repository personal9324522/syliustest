<?php

declare(strict_types=1);

namespace App\Repository;

use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository as BaseProductRepository;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;

class ProductRepository extends BaseProductRepository
{
    public function findAllByOnHand(string $locale = null, int $limit = null, ?string $category): array
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('variant')
            ->addSelect('translation')
            ->leftJoin('o.variants', 'variant')
            ->leftJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->addOrderBy('variant.onHand', 'ASC')
            ->setParameter('locale', $locale)
        ;
        if (null !== $limit) {
        $qb->setMaxResults($limit);
        }
        if (null !== $category) {
            $qb->innerJoin('o.productTaxons', 'productTaxons')
                ->innerJoin('productTaxons.taxon', 'taxon')
                ->andWhere('taxon.code = :category')
                ->setParameter('category', $category)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function findByOption(ProductOptionInterface $option): array
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('variant')
            ->addSelect('channelPricings')
            ->addSelect('optionValues')
            ->leftJoin('o.variants', 'variant')
            ->leftJoin('variant.channelPricings', 'channelPricings')
            ->leftJoin('variant.optionValues', 'optionValues')
            ->innerJoin('o.options', 'option')
            ->andWhere('option = :option')
            ->setParameter('option', $option)
        ;
        return $qb->getQuery()->getResult();
    }

    public function findForDetail(int $id): ProductInterface
    {
        $qb = $this->createQueryBuilder('o')
            ->addSelect('variant')
            ->addSelect('channelPricings')
            ->addSelect('optionValues')
            ->leftJoin('o.variants', 'variant')
            ->leftJoin('variant.channelPricings', 'channelPricings')
            ->leftJoin('variant.optionValues', 'optionValues')
            ->innerJoin('o.options', 'option')
            ->andWhere('o.id = :id')
            ->setParameter('id', $id)
        ;
        return $qb->getQuery()->getSingleResult();
    }
}
