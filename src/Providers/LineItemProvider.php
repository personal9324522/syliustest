<?php

declare(strict_types=1);

namespace App\Providers;

use FluxSE\SyliusPayumStripePlugin\Provider\LineItemImagesProviderInterface;
use FluxSE\SyliusPayumStripePlugin\Provider\LineItemProviderInterface;
use FluxSE\SyliusPayumStripePlugin\Provider\LinetItemNameProviderInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\PaymentInterface;

final class LineItemProvider
{
    public function __construct(
        private LineItemImagesProviderInterface $lineItemImagesProvider,
        private LinetItemNameProviderInterface $lineItemNameProvider
    ) {
    }

    public function getLineItem(OrderItemInterface $orderItem, PaymentInterface $payment): ?array
    {
        /** @var OrderInterface|null $order */
        $order = $orderItem->getOrder();

        if (null === $order) {
            return null;
        }
        // récupération du montant total de l'item
        $amount = $orderItem->getTotal();
        // Si la commande n'est pas totalement payée (paiement par acompte)
        if (!$order->isTotallyPaid()) {
            // si l'acompte a déjà été payé
            if ($payment->getAmount() === $order->getTotalRestant()) {
                // on récupère le montant restant à payer
                $amount = $orderItem->getTotal() - $orderItem->getAcompte();
            } else {
                // sinon on récupère le montant de l'acompte
                $amount = $orderItem->getAcompte();
            }
        }

        $priceData = [
            'unit_amount' => $amount,
            'currency' => $order->getCurrencyCode(),
            'product_data' => [
                'name' => $this->lineItemNameProvider->getItemName($orderItem),
                'images' => $this->lineItemImagesProvider->getImageUrls($orderItem),
            ],
        ];

        return [
            'price_data' => $priceData,
            'quantity' => 1,
        ];
    }
}
