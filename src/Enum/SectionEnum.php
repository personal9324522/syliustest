<?php

namespace App\Enum;

class SectionEnum
{
    const ARTICLE = 'article';
    const BANNER = 'banner';
    const BLOG = 'blog';
    const BRAND = 'brand';
    const ABOUT = 'about';
    const FAQ_SHOP = 'faq_shop';
    const MAIN_FAQ = 'main_faq';
}
