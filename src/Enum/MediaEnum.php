<?php

declare(strict_types=1);

namespace App\Enum;

class MediaEnum
{
    const VIDEO = 'video';
    const IMAGE = 'image';
}
