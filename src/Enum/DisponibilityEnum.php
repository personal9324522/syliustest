<?php

declare(strict_types=1);

namespace App\Enum;

final class DisponibilityEnum
{
    const DISPO = 'DISPONIBLE';

    const INDISPONIBLE = 'RESERVATIONS CLOSES';

    const PRE_RESERVATION = 'A RESERVER POUR';

    public static function getAll(): array
    {
        return [
            self::DISPO => self::DISPO,
            self::INDISPONIBLE => self::INDISPONIBLE,
            self::PRE_RESERVATION => self::PRE_RESERVATION,
        ];
    }
}
