<?php

declare(strict_types=1);

namespace App\Enum\Mails;

class VariableEnum
{

    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const CUSTOMER_MAIL = 'customerMail';
    const CUSTOMER_TEL = 'customerTel';
    const PRODUCT_NAME = 'productName';
    const PRODUCT_PRICE = 'productPrice';
    const ORDER_NUM = 'order_num';
    const ORDER_PRICE = 'order_price';
    const ORDER_ACOMPTE = 'order_acompte';

    const VARIABLES_CUSTOMER = [
        self::VARIABLES_TRADUCTIONS[self::FIRSTNAME],
        self::VARIABLES_TRADUCTIONS[self::LASTNAME],
        self::VARIABLES_TRADUCTIONS[self::CUSTOMER_MAIL],
        self::VARIABLES_TRADUCTIONS[self::CUSTOMER_TEL],
    ];

    const VARIABLES_ORDER = [
        self::VARIABLES_TRADUCTIONS[self::ORDER_NUM],
        self::VARIABLES_TRADUCTIONS[self::ORDER_PRICE],
        self::VARIABLES_TRADUCTIONS[self::ORDER_ACOMPTE],
    ];

    const VARIABLES_PRODUCT= [
        self::VARIABLES_TRADUCTIONS[self::PRODUCT_NAME],
        self::VARIABLES_TRADUCTIONS[self::PRODUCT_PRICE],
    ];

    const VARIABLES_TRADUCTIONS = [
        self::FIRSTNAME => 'Prenom Client',
        self::LASTNAME => 'Nom Client',
        self::CUSTOMER_MAIL => 'Mail Client',
        self::CUSTOMER_TEL => 'Numero de telephone Client',
        self::PRODUCT_NAME => 'Nom espèce',
        self::PRODUCT_PRICE => 'Prix espece',
        self::ORDER_NUM => 'Numero de commande',
        self::ORDER_PRICE => 'Montant Commande',
        self::ORDER_ACOMPTE => 'Montant Acompte',
    ];

    public static function getAttributeName(string $variable) : string
    {
        return match ($variable) {
            self::VARIABLES_TRADUCTIONS[self::FIRSTNAME] => 'firstname',
            self::VARIABLES_TRADUCTIONS[self::LASTNAME] => 'lastname',
            self::VARIABLES_TRADUCTIONS[self::CUSTOMER_MAIL] => 'email',
            self::VARIABLES_TRADUCTIONS[self::CUSTOMER_TEL] => 'phoneNumber',
            self::VARIABLES_TRADUCTIONS[self::PRODUCT_NAME] => 'productName',
            self::VARIABLES_TRADUCTIONS[self::PRODUCT_PRICE] => 'total',
            self::VARIABLES_TRADUCTIONS[self::ORDER_NUM] => 'number',
            self::VARIABLES_TRADUCTIONS[self::ORDER_PRICE] => 'total',
            self::VARIABLES_TRADUCTIONS[self::ORDER_ACOMPTE] => 'totalAcompte',
            default => '',
        };
    }

    public static function getTraduction(string $variable) : string
    {
        Return '';
    }
}
