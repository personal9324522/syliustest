<?php

declare(strict_types=1);

namespace App\Actions;

use App\Providers\LineItemProvider;
use FluxSE\SyliusPayumStripePlugin\Action\ConvertPaymentActionInterface;
use FluxSE\SyliusPayumStripePlugin\Provider\DetailsProviderInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\Convert;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\PaymentInterface;

final class ConvertPaymentAction implements ConvertPaymentActionInterface
{


    public function __construct(
        private DetailsProviderInterface $detailsProvider,
        private LineItemProvider $lineItemProvider
    ) {
    }

    /** @param Convert $request */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getSource();
        /** @var OrderInterface $order */
        $order = $payment->getOrder();

        $details = $this->detailsProvider->getDetails($order);
        // Ajout des items de la commande
        $details['line_items'] = array_map(
            fn (OrderItemInterface $orderItem) => $this->lineItemProvider->getLineItem($orderItem, $payment),
            $order->getItems()->toArray()
        );

        $request->setResult($details);
    }

    public function supports($request): bool
    {
        return
            $request instanceof Convert &&
            $request->getSource() instanceof PaymentInterface &&
            $request->getTo() === 'array'
        ;
    }
}
