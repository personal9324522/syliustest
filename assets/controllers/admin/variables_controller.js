import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    connect() {
        console.log('variables-controller connected');
        // récupération des éléments du DOM
        this._element = $(this.element);
        this.toolbar = this._element.find('#cke_1_toolbox');
        const html =
            "<span class=\"cke_toolbar cke_toolbar_last ui hidden element\" role=\"toolbar\" data-action=\"click->admin--variables#select\">\n" +
            "    <span class=\"cke_toolbar_start\"></span>\n" +
            "    <span class=\"cke_toolgroup\" role=\"presentation\">\n" +
            "        <a class=\"cke_button\" title=\"Insérer une variable\" >\n" +
            "            <span class=\"cke_button_icon\"><i class=\"ui mini tags icon\"></i></span>\n" +
            "        </a>\n" +
            "    </span>\n" +
            "    <span class=\"cke_toolbar_end\"></span>\n" +
            "</span>\n"
        this.toolbar.append(html);
        console.log(this.toolbar);
    }
}
