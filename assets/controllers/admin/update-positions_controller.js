import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    static values = {
        url: String
    }

    connect() {
        console.log('update-positions connected');
    }

    update(event) {
        // récupération des données de la liste
        let positions = document.querySelectorAll('.input-position');
        // création d'un tableau vide
        let positionsArray = [];
        // boucle sur les données de la liste
        positions.forEach(function (position) {
            // ajout de l'id et de la position
            positionsArray.push({
                id: position.dataset.id,
                position: position.value
            });
        });
        // envoie de la requete ajax
        $.ajax({
            url: this.urlValue,
            method: 'PUT',
            data: { positions: positionsArray },
            success: () => {
                $(event.currentTarget).removeClass('loading');
                window.location.reload();
            },
        });
    }
}
