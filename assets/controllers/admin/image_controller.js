import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    static targets = ['modalImage', 'image', 'content', 'loader', 'inputImage', 'validBtn'];

    static values = {
        url: String
    }
    connect() {
        console.log('Image controller connecté');
        // récupération des éléments du DOM
        this._element = $(this.element);
        this._modal = $(this.modalImageTarget);
        this._image = $(this.imageTarget);
        this._content = $(this.contentTarget);
        this._loader = $(this.loaderTarget);
        this._inputImage = $(this.inputImageTarget);
        this._validBtn = $(this.validBtnTarget);
        // initialisation de la modal
        this._modal.modal({
            autofocus: false,
            dimmerSettings: {
                // opacity: 0.25
            },
            onHide: () => {
                // suppression de la surbrillance
                this._content.find('.card.selected').removeClass('selected');
            }
        });

        this._validBtn.on('click', this.valid.bind(this));
    }

    show(event) {
        // ajout de la video dan sla modal
        this._modal.modal('show');
        if (!this._modal.find('img').length > 0) {
            // récupération des images
            $.ajax({
                url: this.urlValue,
                method: 'GET',
                success: function (data) {
                    // ajout du contenu dans la modal
                    this._modal.find('.content').append(data.html);
                    // fin du chargement
                    this._loader.hide();
                    this._content.find('.card').each((index, card) => {
                        card.addEventListener('click', this.select.bind(this));
                    });
                }.bind(this),
                error: (error) => {
                    console.err(error);
                }
            });
        }
    }

    select(event) {
        // récupération de toutes les cards et suppression de la surbrillance
        this._content.find('.card').each((index, card) => {
            // suppression de la surbrillance
            $(card).removeClass('selected');
        });
        // récupération de la card
        let card = $(event.currentTarget);
        // mise en surbrillance de la card
        card.addClass('selected');
    }

    valid(event) {
        console.log('valid');
        // récupération du bouton
        let button = $(event.currentTarget);
        // mise en chargement du bouton
        button.addClass('loading');
        // récupération de la card sélectionnée
        let card = this._content.find('.card.selected');
        // récupération de l'image
        let image = card.find('img');
        // récupération de l'url de l'image
        let url = image.attr('src');
        // mise à jour de la preview
        this._image.attr('src', url);
        // mise à jour de l'input
        this._inputImage.val(url);
        // fermeture de la modal
        this._modal.modal('hide');
        // suppresion de la surbrillance
        card.removeClass('selected');
        // fin du chargement du bouton
        button.removeClass('loading');
    }
}
