import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    static targets = ['preview', 'image', 'video', 'input'];

    connect() {
        console.log('file controller connected');
        // récupération des éléments du DOM
        this._inputFile = $('input[type="file"]');
        this._preview = $(this.previewTarget);
        this._image = $(this.imageTarget);
        this._video = $(this.videoTarget);
        this._input = $(this.inputTarget);
        // récupération du bouton de validation
        this._button = $(document.querySelector('button[type="submit"]'));

        this.title = this._input.val();
        this.source = this._image.attr('src') ? this._image.attr('src') : this._video.find('source').attr('src');
        this.isUpdate = !!(this.title && this.source);
        this.check();
    }

    change(event) {
        // récupération du fichier
        const file = event.target.files[0];
        // récupération de l'URL du fichier
        let src = URL.createObjectURL(file);
        // si le fichier est une image
        if (file.type.match('image/*')) {
            this._image.attr('src', src);
            // affichage de la balise img
            this._image.show();
            this._video.hide();

        } else if (file.type.match('video/*')) {
            // Supprimer l'élément vidéo existant
            this._video.remove();
            // Créer un nouvel élément vidéo
            this._video = $('<video width="400" controls style="display: block;" data-admin--file-target="video"></video>');
            this._video.append('<source src="' + src + '" type="' + file.type + '">');
            // Insérer le nouvel élément vidéo dans la preview
            this._preview.append(this._video);
            // Cacher l'image
            this._image.hide();
        }
        this.check();
    }

    check() {
        // si il s'agit d'une mise à jour
        if (this.isUpdate) {
            // si les champs sont différents et qu'il y a un titre
            if (this._input.val() && this.isDifferent()) {
                this._button.removeClass('disabled');
            } else {
                this._button.addClass('disabled');
            }
        } else {
            // si il y a un titre et un fichier
            if (this._input.val() && !this.isEmptyMedia()) {
                this._button.removeClass('disabled');
            } else {
                this._button.addClass('disabled');
            }
        }

    }

    isEmptyMedia() {
        // si il n'y a pas de fichier
        if (this._image.attr('src') === '' || this._video.find('source').attr('src') === '') {
            if (this._inputFile.val() === '') {
                    return true;
            }
        }
        return false;
    }

    isDifferent() {
        // vérifie si les champs sont différents
        return this._input.val() !== this.title || (this._video.find('source').attr('src') !== this.source || this._image.attr('src') !== this.source);
    }
}
