import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    static targets = [ 'uploadFileItem', 'uploadFileList']

    connect() {
        // récupération des éléments du DOM
        this._inputFile = $('input[type="file"]');
        this._uploadFileItem = $(this.uploadFileItemTarget)
        this._uploadFileList = $(this.uploadFileListTarget)
    }

    upload() {
        this._inputFile.trigger('click');
    }

    change(event) {
        const files = Array.from(this._inputFile[0].files);
        this._uploadFileList.html('');
        files.forEach((file) => {
            let newItem = this._uploadFileItem.find('.item').clone();
            newItem.find('.content').html(file.name);
            newItem.removeClass('ui hidden element')
            this._uploadFileList.append(newItem);
        })
    }

   delete(event) {
        console.log($(event.currentTarget))
        // récupération du bouton
       const btn = $(event.currentTarget);
       const url = btn.data('url');
       const filename = btn.data('filename');
       const id = btn.data('id');

       btn.addClass('disabled loading');

       $.ajax({
           url: url,
           method: 'POST',
           data: {
               filename: filename,
               id: id,
           },
           success: () => {
               // suppression de la ligne
               btn.parent().parent().transition({
                   animation: 'flash',
                   duration: '1s',
                   onComplete: () => {
                       btn.parent().parent().remove();
                   }
               }).remove();
           },
           error: (err) => {
               console.error(err)
           }
       })
   }
}
