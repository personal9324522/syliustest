import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller{

    static targets = ['modal', 'video'];

    connect() {
        console.log('video controller connected');
        // récupération des éléments du DOM
        this._element = $(this.element);
        this._modal = $(this.modalTarget)
        this._video = $(this.videoTarget);
        this.stream = null;
        this.tracks = null;

        // initialisation de la modal
        this._modal.modal({
            autofocus: false,
            dimmerSettings: {
                onHide: () => {
                    this._video[0].pause();
                    // suppression de la vidéo de la modal
                    this._video.find('source').attr('src', '');
                }
            }
        });
    }

     show(event) {
         // si l'évènement précédent est un swipe, on ne fait rien
        if ($(event.currentTarget).css('pointer-events') === 'none') {
            console.log('swipe');
            return;
        }
        // récupération de la source de la vidéo
        let source = $(event.currentTarget).data('path');
        // remplacement de la source de la vidéo dans les balises source
        this._video.find('source').attr('src', source);
        // rechargement de la vidéo
        this._video[0].load();
        // ajout de la video dan sla modal
        this._modal.modal('show');
    }
}
