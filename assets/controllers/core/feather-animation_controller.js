import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    connect() {
        console.log('Feather controller connected.')

        // récupération des div des images masquées
        const images = document.querySelectorAll('.image');
        // Création d'un observateur IntersectionObserver
        const observer = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    // L'image est maintenant visible dans la fenêtre
                    const image = entry.target;
                    this.startAnimation(image);
                    observer.unobserve(entry.target);
                }
            });
        }, { threshold: 0.5 }); // Vous pouvez ajuster le seuil selon vos besoins

        // Associiation de l'observateur à chaque image
        images.forEach((image) => {
            observer.observe(image);
        });

    }

    startAnimation(element) {
        element.classList.add('animation');
    }
}
