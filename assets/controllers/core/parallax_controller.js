import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    static targets = ['parallax'];

    connect() {
        this._parallax = $(this.parallaxTargets);
        console.log('Parallax controller connected.');
        window.addEventListener('scroll', this.startAnimation.bind(this));
    }

    startAnimation(event) {
        this._parallax.each( (index, element) => {
            const value = $(element).data('value');

            element.style.transform = "translateY(" + value * (window.scrollY / 10) + "px)";
        });
    }
}
