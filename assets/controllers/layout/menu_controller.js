import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets= ['menu'];

    connect() {
        this._menu = $(this.menuTarget);
    }

    open() {
        this._menu.toggleClass('hidden');
    }

    close() {
        this._menu.toggleClass('hidden');
    }

}
