import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    scrollToAnchor(event) {
        let anchor = $(event.currentTarget).data('anchor');
        let anchorSection = document.getElementById(anchor);
        anchorSection.scrollIntoView({ behavior: 'smooth' });
    }
}
