CKEDITOR.plugins.add('variables_button', {
    icons: 'variable',
    init: function (editor) {
        // Définissez le comportement de votre bouton ici
        editor.addCommand('variables_button', {
            exec: function (editor) {
                console.log('variable added')
            }
        });

        editor.ui.addButton('VariablesButton', {
            label: 'Insérer des variables',
            command: 'variables_button',
            toolbar: 'insert'
        });
    }
});
