import './styles/app.scss';
// start the Stimulus application
import './bootstrap';
// importy semantic
import 'semantic-ui-css/semantic.min.css';
import 'semantic-ui-css/semantic.min.js';

require('@fortawesome/fontawesome-free/css/all.min.css');

$(document).ready(() => {

    $('.cart.button')
        .popup({
            popup: $('.cart.popup'),
            on: 'click',
            position   : 'bottom left',
            onUnplaceable() {
                let href = $('#sylius-go-to-cart').attr('href');
                if(href) {
                    window.location.href = href;
                }
            },
            silent: true,
        });
});
