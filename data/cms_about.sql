INSERT INTO sylius.cms_about (about_id, about_title, about_description, about_section) VALUES (1, 'qui sommes-nous?', 'Le centre d\'élevage " Un amour de perroquet " est situé en France sur la côté Atlantique (Vendée 85).

	
Il est spécialisé dans l\'élevage des perroquets, des plus communs au plus menacés. Nous sommes partenaires de programmes internationaux pour la préservation d\'espèces rares.

Nous proposons 2 types de perroquets :

- les oiseaux de compagnie, élevés à la main dès l’éclosion. Ces oiseaux, habitués à l’homme, ont toutes les qualités pour vivre fidèlement à vos côtés. Ils viendront sur vous, pourront apprendre à siffler, parler, chanter !… Nous les vendons sevrés, c’est-à-dire qu’ils savent s’alimenter seuls.

- les oiseaux d’espèces plus rares, généralement destinés aux éleveurs et à la reproduction pour la pérennité de l’espèce en captivité.



Le Centre d’Elevage «Un Amour de Perroquet » possède un grand nombre de couples reproducteurs et détient de multiples espèces. Nous distribuons des perroquets dans toute l’Europe et avons des références avec les collections zoologiques les plus prestigieuses.

Bonne visite de notre site, nous espérons, au travers de nos galeries photos unique, vous transmettre notre passion pour ces animaux merveilleux !
Le curateur.', null);
