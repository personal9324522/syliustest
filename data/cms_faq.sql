INSERT INTO sylius.cms_faq (faq_id, faq_question, faq_answer, faq_section, faq_position) VALUES (1, 'Je vérifie la disponibilité', 'Si vous êtes prêts à adopter un de nos perroquets, prenez contact avec nous pour vérifier la disponibilité par e-mail : amour.de.perroquet@free.fr  ou par téléphone au 06.03.98.57.67', 'faq_shop', 2);
INSERT INTO sylius.cms_faq (faq_id, faq_question, faq_answer, faq_section, faq_position) VALUES (2, 'Comment régler ma réservation?', 'Une fois la disponibilité validée vous pourrez payer directement en ligne, avec votre carte bancaire :
<div>
<ul>
    <li> l\'acompte de réservation (100 à 200 € selon l\'espèce)</li>
    <li> le solde en 1 fois ou bien (en 3 fois sans frais, pour cela nous consulter)</li>
</ul>
</div>
Pour cela, choisissez sur cette page la rubrique qui vous intéresse... amazones, aras, cacatoès, pionus, gris du gabon etc...

Nous vous ferons confirmation de l\'arrivée du paiement, et conviendrons ensemble de la date de départ.', 'faq_shop', 3);
INSERT INTO sylius.cms_faq (faq_id, faq_question, faq_answer, faq_section, faq_position) VALUES (3, 'Suis-je prêt à adopter un oiseau?', 'L’achat d’un perroquet comme oiseau de compagnie est une décision importante qui ne peut et ne doit pas se faire sur un coup de tête.
Un perroquet est un être vivant doté de sensibilité et d\'intelligence qui peut vivre des dizaines d’années, il mérite d’être logé dans de bonnes conditions et nécessite qu’on lui consacre quotidiennement du temps et de l\'attention. 
Prenez du temps pour y réfléchir donc le temps avant de prendre une décision.', 'faq_shop', 1);
