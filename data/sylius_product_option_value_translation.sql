INSERT INTO sylius.sylius_product_option_value_translation (id, translatable_id, value, locale) VALUES (1, 1, 'Pack sanitaire 80 euros', 'fr_FR');
INSERT INTO sylius.sylius_product_option_value_translation (id, translatable_id, value, locale) VALUES (2, 2, 'Sans pack sanitaire', 'fr_FR');
INSERT INTO sylius.sylius_product_option_value_translation (id, translatable_id, value, locale) VALUES (3, 3, 'Femelle', 'fr_FR');
INSERT INTO sylius.sylius_product_option_value_translation (id, translatable_id, value, locale) VALUES (4, 4, 'Mâle', 'fr_FR');
INSERT INTO sylius.sylius_product_option_value_translation (id, translatable_id, value, locale) VALUES (5, 5, 'Indifferent', 'fr_FR');
